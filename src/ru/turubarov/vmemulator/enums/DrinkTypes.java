package ru.turubarov.vmemulator.enums;

/**
 * �������� ��������, ����������� � ��������.
 * 
 * @author ���������
 * 
 */
public enum DrinkTypes {
	/**
	 * ���.
	 */
	TEA {
		public int cost() {
			return 13;
		}
	},
	/**
	 * ����.
	 */
	COFFEE {
		public int cost() {
			return 18;
		}
	},
	/**
	 * ���� � �������.
	 */
	MILK_COFFEE {
		public int cost() {
			return 21;
		}
	},
	/**
	 * ���.
	 */
	JUICE {
		public int cost() {
			return 35;
		}
	};
	public abstract int cost();
}
