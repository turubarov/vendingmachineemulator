package ru.turubarov.vmemulator.enums;

/**
 * ������������ ����� ������ ����������.
 * 
 * @author ���������
 * 
 */
public enum CoinTypes {
	/**
	 * ������ 1 �.
	 */
	COIN1 {
		public int value() {
			return 1;
		}
	},
	/**
	 * ������ 2 �.
	 */
	COIN2 {
		public int value() {
			return 2;
		}
	},
	/**
	 * ������ 5 �.
	 */
	COIN5 {
		public int value() {
			return 5;
		}
	},
	/**
	 * ������ 10 �.
	 */
	COIN10 {
		public int value() {
			return 10;
		}
	};
	public abstract int value();

}
