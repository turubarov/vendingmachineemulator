package ru.turubarov.vmemulator.mappers;

import org.json.JSONException;
import org.json.JSONObject;

import ru.turubarov.vmemulator.enums.CoinTypes;
import ru.turubarov.vmemulator.enums.DrinkTypes;
import ru.turubarov.vmemulator.model.Drinks;
import ru.turubarov.vmemulator.model.User;
import ru.turubarov.vmemulator.model.VendingMachine;
import ru.turubarov.vmemulator.model.Wallet;

/**
 * ����� ��� ��������� JSON, ������������ ������ ������ {@link VendingMachine}
 * @author ���������
 *
 */
public class DataJsonMapper {
	
	/**
	 * ��������� ������ JSON ��� �������� {@code vendingMachine} � {@link User}.
	 * @param vendingMachine
	 * @return
	 */
	public static String getJson(VendingMachine vendingMachine, User user) {
		JSONObject obj = new JSONObject();
		Wallet vmWallet = vendingMachine.getWallet();
		Drinks drinks = vendingMachine.getDrinks();
		Wallet uWallet = user.getWallet();
		try {
			obj.put("vm_count_coin1", vmWallet.getCountCoins(CoinTypes.COIN1));
			obj.put("vm_count_coin2", vmWallet.getCountCoins(CoinTypes.COIN2));
			obj.put("vm_count_coin5", vmWallet.getCountCoins(CoinTypes.COIN5));
			obj.put("vm_count_coin10", vmWallet.getCountCoins(CoinTypes.COIN10));
			obj.put("tea_count", drinks.getCountDrinks(DrinkTypes.TEA));
			obj.put("coffee_count", drinks.getCountDrinks(DrinkTypes.COFFEE));
			obj.put("coffee_milk_count", drinks.getCountDrinks(DrinkTypes.MILK_COFFEE));
			obj.put("juice_count", drinks.getCountDrinks(DrinkTypes.JUICE));
			obj.put("summa", vendingMachine.getSumma());
			obj.put("user_count_coin1", uWallet.getCountCoins(CoinTypes.COIN1));
			obj.put("user_count_coin2", uWallet.getCountCoins(CoinTypes.COIN2));
			obj.put("user_count_coin5", uWallet.getCountCoins(CoinTypes.COIN5));
			obj.put("user_count_coin10", uWallet.getCountCoins(CoinTypes.COIN10));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj.toString();
	}
	
}
