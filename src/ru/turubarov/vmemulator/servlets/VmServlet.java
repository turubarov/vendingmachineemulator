package ru.turubarov.vmemulator.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.turubarov.vmemulator.enums.CoinTypes;
import ru.turubarov.vmemulator.enums.DrinkTypes;
import ru.turubarov.vmemulator.mappers.DataJsonMapper;
import ru.turubarov.vmemulator.model.User;
import ru.turubarov.vmemulator.model.VendingMachine;
import ru.turubarov.vmemulator.model.Wallet;

/**
 * Servlet implementation class VmServlet
 */
public class VmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VendingMachine vendingMachine;
	private User user;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VmServlet() {
		super();
		vendingMachine = new VendingMachine();
		user = new User();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		String param = request.getParameter("button_id");
		if ("coin1".equals(param)) {
			makePayment(CoinTypes.COIN1);
		} else if ("coin2".equals(param)) {
			makePayment(CoinTypes.COIN2);
		} else if ("coin5".equals(param)) {
			makePayment(CoinTypes.COIN5);
		} else if ("coin10".equals(param)) {
			makePayment(CoinTypes.COIN10);
		} else if ("tea".equals(param)) {
			makeSale(DrinkTypes.TEA);
		} else if ("coffee".equals(param)) {
			makeSale(DrinkTypes.COFFEE);
		} else if ("milk_coffee".equals(param)) {
			makeSale(DrinkTypes.MILK_COFFEE);
		} else if ("juice".equals(param)) {
			makeSale(DrinkTypes.JUICE);
		} else if ("return_money".equals(param)) {
			giveChange();
		}
		String res = DataJsonMapper.getJson(vendingMachine, user);
		response.getWriter().write(res);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private void makePayment(CoinTypes coinType) {
		user.giveCoin(coinType);
		vendingMachine.receiveCoin(coinType);
	}

	private void makeSale(DrinkTypes drinkType) {
		vendingMachine.saleDrink(drinkType);
	}
	
	private void giveChange() {
		Wallet change = vendingMachine.giveChange();
		user.getWallet().addWallet(change);
	}

}
