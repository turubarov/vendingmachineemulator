package ru.turubarov.vmemulator.model;

import java.util.HashMap;

import ru.turubarov.vmemulator.enums.CoinTypes;

/**
 * ����� ����� ������� �����������.
 * 
 * @author ���������
 * 
 */
public class Wallet {
	private HashMap<CoinTypes, Integer> countCoins;

	public Wallet() {
		countCoins = new HashMap<CoinTypes, Integer>();
		for (int i = 0; i < CoinTypes.values().length; i++) {
			CoinTypes type = CoinTypes.values()[i];
			countCoins.put(type, 0);
		}
	}

	public Wallet(Wallet other) {
		this.countCoins = new HashMap<CoinTypes, Integer>(other.countCoins);
	}

	/**
	 * ���������� ���������� ����� ������������ �����������.
	 * 
	 * @param coinType
	 *            ����������� ������
	 * @param count
	 *            ���������� �����
	 */
	public void setCountCoins(CoinTypes coinType, Integer count) {
		countCoins.put(coinType, count);
	}

	/**
	 * �������� ���������� ����� ������������ �����������.
	 * 
	 * @param coinType
	 *            ����������� ������
	 * @return ���������� �����
	 */
	public Integer getCountCoins(CoinTypes coinType) {
		return countCoins.get(coinType);
	}

	/**
	 * ��������� ���������� ����� � ������ �� ���� �����.
	 * 
	 * @param coinTypes
	 *            ����������� ������
	 * @return ���������� �����, ���������� � ������
	 */
	public Integer decrementCoins(CoinTypes coinTypes) {
		Integer val = countCoins.get(coinTypes);
		if (val > 0) {
			val--;
			countCoins.put(coinTypes, val);
		}
		return val;
	}

	/**
	 * ��������� ���������� ����� � ������ �� ���� �����.
	 * 
	 * @param coinTypes
	 *            ����������� ������
	 * @return ���������� �����, ���������� � ������
	 */
	public Integer incrementCoins(CoinTypes coinTypes) {
		Integer val = countCoins.get(coinTypes);
		val++;
		countCoins.put(coinTypes, val);
		return val;
	}

	/**
	 * �������� �� ������ ����������� ���������� ����� ��������� �����������.
	 * 
	 * @param coinType
	 *            ����������� ������
	 * @param count
	 *            ���������� �����
	 * @return ���������� ����� � ������ ����� ���������� ��������
	 */
	public Integer giveCoins(CoinTypes coinTypes, Integer count) {
		Integer val = countCoins.get(coinTypes);
		if (val - count >= 0) {
			val -= count;
			countCoins.put(coinTypes, val);
		}
		return val;
	}

	/**
	 * �������� � ����� ����������� ���������� ����� ��������� �����������.
	 * 
	 * @param coinType
	 *            ����������� ������
	 * @param count
	 *            ���������� �����
	 * @return ���������� ����� � ������ ����� ���������� ��������
	 */
	public Integer addCoins(CoinTypes coinTypes, Integer count) {
		Integer val = countCoins.get(coinTypes);
		val += count;
		countCoins.put(coinTypes, val);
		return val;
	}

	/**
	 * �������� ������ ����� �����.
	 * 
	 * @param wallet
	 */
	public void addWallet(Wallet anotherWallet) {
		for (int i = 0; i < CoinTypes.values().length; i++) {
			CoinTypes type = CoinTypes.values()[i];
			addCoins(type, anotherWallet.getCountCoins(type));
		}
	}

}
