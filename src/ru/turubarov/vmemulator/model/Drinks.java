package ru.turubarov.vmemulator.model;

import java.util.HashMap;

import ru.turubarov.vmemulator.enums.DrinkTypes;

/**
 * ����� ��������� �������, ���������� ���������.
 * @author ���������
 *
 */
public class Drinks {
	private HashMap<DrinkTypes, Integer> countDrinks;
	public Drinks() {
		countDrinks = new HashMap<DrinkTypes, Integer>();
	}

	public void setCountDrinks(DrinkTypes coinType, Integer count) {
		countDrinks.put(coinType, count);
	}

	public Integer getCountDrinks(DrinkTypes coinType) {
		return countDrinks.get(coinType);
	}
	
	/**
	 * ���������� ���������� ������� ��������� ���� �� ���� ������.
	 * @param drinkType ��� �������
	 */
	public Integer decrementDrinks(DrinkTypes drinkType) {
		Integer val = countDrinks.get(drinkType);
		if (val > 0) {
			val--;
			countDrinks.put(drinkType, val);
		}
		return val; 
	}
	
}
