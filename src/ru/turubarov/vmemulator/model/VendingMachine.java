package ru.turubarov.vmemulator.model;

import ru.turubarov.vmemulator.enums.CoinTypes;
import ru.turubarov.vmemulator.enums.DrinkTypes;

/**
 * ������� �� ������� ��������.
 * 
 * @author ���������
 * 
 */
public class VendingMachine {
	private int summa;
	private Wallet wallet;
	private Drinks drinks;
	private final Integer DEFAULT_COUNT_COIN1 = 100;
	private final Integer DEFAULT_COUNT_COIN2 = 100;
	private final Integer DEFAULT_COUNT_COIN5 = 100;
	private final Integer DEFAULT_COUNT_COIN10 = 100;

	public VendingMachine() {
		wallet = new Wallet();
		wallet.setCountCoins(CoinTypes.COIN1, DEFAULT_COUNT_COIN1);
		wallet.setCountCoins(CoinTypes.COIN2, DEFAULT_COUNT_COIN2);
		wallet.setCountCoins(CoinTypes.COIN5, DEFAULT_COUNT_COIN5);
		wallet.setCountCoins(CoinTypes.COIN10, DEFAULT_COUNT_COIN10);
		drinks = new Drinks();
		drinks.setCountDrinks(DrinkTypes.TEA, 10);
		drinks.setCountDrinks(DrinkTypes.COFFEE, 20);
		drinks.setCountDrinks(DrinkTypes.MILK_COFFEE, 20);
		drinks.setCountDrinks(DrinkTypes.JUICE, 15);
		summa = 0;
	}

	/**
	 * ��������� ������ ���������.
	 * 
	 * @param type
	 *            ��� ���������� ������
	 */
	public Integer receiveCoin(CoinTypes type) {
		summa += type.value();
		return wallet.incrementCoins(type);
	}

	/**
	 * ������� ����� ������ ������� ���������.
	 * 
	 * @param type
	 *            ��� ������������ �������
	 * @return ���������� ������ ������� ���� {@code type}, ���������� �
	 *         �������� ����� �������, ��� -1, ���� � �������� ������������
	 *         �����.
	 */
	public Integer saleDrink(DrinkTypes drinkType) {
		if (summa >= drinkType.cost()) {
			summa -= drinkType.cost();
			return getDrinks().decrementDrinks(drinkType);
		} else {
			return -1;
		}
	}

	/**
	 * ������ ����� � ���� ������ �����.
	 * 
	 * @return
	 */
	public Wallet giveChange() {
		Wallet result = new Wallet();
		int needSum;
		int haveSum;
		int needCoinsCount;
		int haveCoinsCount;
		int valueCoin;
		int giveCoinsCount;
		int startSumma = summa;
		Wallet startWallet = new Wallet(wallet);
		for (int i = CoinTypes.values().length - 1; i >= 0; i--) {
			CoinTypes type = CoinTypes.values()[i];
			valueCoin = type.value();
			needCoinsCount = (int) Math.floor(summa / type.value());
			haveCoinsCount = wallet.getCountCoins(type);
			needSum = needCoinsCount * valueCoin;
			haveSum = haveCoinsCount * valueCoin;
			if (needSum > haveSum) {
				giveCoinsCount = haveCoinsCount;
			} else {
				giveCoinsCount = needCoinsCount;
			}
			wallet.giveCoins(type, giveCoinsCount);
			result.setCountCoins(type, giveCoinsCount);
			summa -= giveCoinsCount * valueCoin;
			if (summa == 0) {
				break;
			}
		}
		// ���� ������ �������� �� ��� ����������, �������� ������ ������ �����
		// ��������� ������� ������������� ����������������
		if (summa > 0) {
			// ��������������� ��������� ������
			summa = startSumma;
			wallet = startWallet;
			result = new Wallet();
			int[] m = new int[summa + 1]; // ������ ������������� ��������
			int countNomimals = CoinTypes.values().length;
			int[] nominals = new int[countNomimals];
			int[] countCoins = new int[countNomimals];
			int[][] usedCoins = new int[countNomimals][summa + 1];
			// ��������� ������ � ������� � ������� ��� �����������;
			// ��������� � HashMap ����� ��������� O(N) � ������ ������
			for (int i = 0; i < countNomimals; i++) {
				nominals[i] = CoinTypes.values()[i].value();
				countCoins[i] = wallet.getCountCoins(CoinTypes.values()[i]);
			}
			// ��������� �������� �������
			m[0] = 0;
			for (int i = 1; i <= summa; i++) {
				int maxVal = 0;
				int maxInd = -1;
				for (int j = 0; j < countNomimals; j++) {
					if (nominals[j] <= i
							&& (nominals[j] + m[i - nominals[j]]) > maxVal
							&& countCoins[j] - usedCoins[j][i - nominals[j]] > 0) {
						maxVal = nominals[j] + m[i - nominals[j]];
						maxInd = j;
					}
				}
				m[i] = maxVal;
				if (maxInd != -1) {
					for (int j = 0; j < countNomimals; j++) {
						if (j != maxInd) {
							usedCoins[j][i] = usedCoins[j][i - nominals[maxInd]];
						} else {
							usedCoins[j][i] = usedCoins[j][i - nominals[maxInd]] + 1;
						}
					}
				}
			}
			int giveSum = 0;
			for (int i = 0; i < countNomimals; i++) {
				result.setCountCoins(CoinTypes.values()[i], usedCoins[i][summa]);
				wallet.giveCoins(CoinTypes.values()[i], usedCoins[i][summa]);
				giveSum += usedCoins[i][summa] * CoinTypes.values()[i].value();
			}
			summa -= giveSum;
		}
		return result;
	}

	/**
	 * �����, �������� ������������� � �������.
	 * 
	 * @return
	 */
	public int getSumma() {
		return summa;
	}

	/**
	 * ����� �����, ����������� � ��������.
	 * 
	 * @return
	 */
	public Wallet getWallet() {
		return wallet;
	}

	/**
	 * ����� ��������, ����������� � ��������.
	 * 
	 * @return
	 */
	public Drinks getDrinks() {
		return drinks;
	}

}
