package ru.turubarov.vmemulator.model;

import ru.turubarov.vmemulator.enums.CoinTypes;

/**
 * ������������ �������� �� ������� ��������.
 * @author ���������
 *
 */
public class User {
	private Wallet wallet;
	private final Integer DEFAULT_COUNT_COIN1 = 10;
	private final Integer DEFAULT_COUNT_COIN2 = 30;
	private final Integer DEFAULT_COUNT_COIN5 = 20;
	private final Integer DEFAULT_COUNT_COIN10 = 15;
	
	public User() {
		wallet = new Wallet();
		getWallet().setCountCoins(CoinTypes.COIN1, DEFAULT_COUNT_COIN1);
		getWallet().setCountCoins(CoinTypes.COIN2, DEFAULT_COUNT_COIN2);
		getWallet().setCountCoins(CoinTypes.COIN5, DEFAULT_COUNT_COIN5);
		getWallet().setCountCoins(CoinTypes.COIN10, DEFAULT_COUNT_COIN10);
	}
	
	/**
	 * ������������ ������ � �������.
	 * @param type ����������� ������
	 */
	public Integer giveCoin(CoinTypes type) {
		return wallet.decrementCoins(type);
	}

	public Wallet getWallet() {
		return wallet;
	}
	
}
